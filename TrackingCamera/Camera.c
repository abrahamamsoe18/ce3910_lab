/*
 * Camera.c
 *
 *  Created on: Apr 4, 2017
 *      Author: Alwin Abraham
 */

#include "Camera.h"
#include "I2C.h"
#include "VGA.h"
#include "Servo.h"

volatile uint8_t* pixel_port = (uint8_t*) 0x80802010;
volatile uint8_t* cam_control = (uint8_t*) 0x80802000;
uint8_t* pb = (uint8_t*) 0x808020d0;
void cameraInit() {
	I2Cinit();
	I2Cwrite(0x11, 8);
	I2Cwrite(0x14, I2Cread(0x14) | 0x20);
	I2Cwrite(0x39, I2Cread(0x39) & 0x40); //PCLK ONLY WHEN HREF
	I2Cwrite(0x05, 0xAA); //Saturation
	I2Cwrite(0x03, 0x00); //Sharpness
}

void readCamera() {
	extern int row;
	extern int column;
	uint8_t* memptr;
	int i = 0;
	int j = 0;
	int count = 0;
	int black_row1;
	int black_col1;
	int black_row2;
	int black_col2;
	int top, right;
	uint8_t pixel;
	while (1) {
		top = 0;
		right = 0;
		black_row1 = -1;
		black_col1 = -1;
		black_row2 = -1;
		black_col2 = -1;
		i = 0;
		j = 0;
		pixel = 0;
		while (*cam_control & 0x04)
			; //VSYNC HIGH
		for (i = 0; i < 144; i++) { //Go through Rows
			while (!(*cam_control & 0x02))
				; //Wait for HREF to Go High
			if (i > 11 && i < 132) { //Block out edges
				memptr = (uint8_t*) (0x80800000 + ((((i - 12) / 2)) << 7)); //Every other row
			}
			for (j = 0; j < 176; j++) { //Go through columns
				count = 0;
				while (!(*cam_control & 0x01))
					; //Wait to PCLK to go High
				pixel = *pixel_port;
				if (j > 7 && j % 2 == 0 && j < 168 && i > 11 && i < 132
						&& i % 2 == 0) { //Block out edges and every other only
					*memptr = pixel; //Save the camera data
					memptr++;
				} else {

					if (j == 27 && pixel > 0xEE && right >= 0) { //left
						right--;
					}
					if (j == 147 && pixel > 0xEE && right <= 0) { //right
						right++;
					}
					if (i == 22 && pixel > 0xEE && top <= 0
							&& (j == 27 || j == 87 || j == 147)) { //up
						top++;
					}
					if (i == 120 && pixel > 0xEE && top >= 0
							&& (j == 27 || j == 87 || j == 147)) { //down
						top--;
					}
				}
				while ((*cam_control & 0x01))
					; //wait for the rest of the PCLK ends
			}
		}
		if (*pb == 2) { //reset with second push button
			row = 66;
			column = 89;
			pan();
			tilt();
		} else if (top > 0) {
			row += 5;
			tilt();
		} else if (top < 0) {
			row -= 5;
			tilt();
		}
		if (right > 0) {
			column += 5;
			pan();
		} else if (right < 0) {
			column -= 5;
			pan();
		}
		top = 0;
		right = 0;
	}
}

