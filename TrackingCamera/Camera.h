/*
 * Camera.h
 *
 *  Created on: Apr 4, 2017
 *      Author: Alwin Abraham
 */
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#ifndef CAMERA_H_
#define CAMERA_H_

extern volatile uint8_t* pixel_port;
extern volatile uint8_t* cam_control;

/**
 * Initialize the camera
 */
void cameraInit();

/**
 * While loop to constantly read the camera
 */
void readCamera();

#endif /* CAMERA_H_ */
