/*
 * I2C.c
 *
 *  Created on: Mar 27, 2017
 *      Author: Alwin Abraham
 */
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "I2C.h"

volatile uint8_t* i2c = (uint8_t*) 0x80802020;

void I2Cinit(void){
	*i2c = 0;//prescale to 99
	*(i2c+1) = 99; //prescale to 99
	*(i2c+2) = *(i2c+2)|0x80;
}

uint8_t I2Caction(uint8_t command){
	*(i2c+4) = command;
	while((*(i2c+4)&2)>>1);
	return !((*(i2c+4)&2)>>7);
}
uint8_t I2Cread(uint8_t regNumber) {
	*(i2c + 3) = 0xC0;
	if (I2Caction(1 << 7 | 1 << 4)) {
		*(i2c + 3) = regNumber;
		if (I2Caction(1 << 6 | 1 << 4)) {
			*(i2c + 3) = 0xC1;
			if (I2Caction(1 << 7 | 1 << 4)) {
				if (I2Caction(1 << 6 | 1 << 5 | 1 << 3)) {
					return *(i2c + 3);
				}
			}
		}
	}
	return -1;
}
void I2Cwrite(uint8_t regNumber, uint8_t value){
	*(i2c + 3) = 0xC0;
		if (I2Caction(1 << 7 | 1 << 4)) {
			*(i2c + 3) = regNumber;
			if (I2Caction(1 << 4)) {
				*(i2c + 3) = value;
				if (I2Caction(1 << 6 | 1 << 4)) {
					//ACK
				}
			}
		}
	return;
}

