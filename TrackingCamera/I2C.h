/*
 * I2C.h
 *
 *  Created on: Mar 27, 2017
 *      Author: Alwin Abraham
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#ifndef I2C_H_
#define I2C_H_

/**
 * Location of I2C on NIOS
 */
extern volatile uint8_t* i2c;

/**
 * Initialize the I2C module
 */
void I2Cinit(void);

uint8_t I2Caction(uint8_t command);
/**
 * Read from I2C
 * @param regNumber - register to read from
 * @return - value from I2C at provided register
 */
uint8_t I2Cread(uint8_t regNumber);

/**
 * Write to I2C
 * @param regNumber - register to write to
 * @param value - value to write to register
 */
void I2Cwrite(uint8_t regNumber, uint8_t value);

#endif /* I2C_H_ */
