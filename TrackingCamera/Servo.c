/*
 * Servo.c
 *
 *  Created on: Mar 16, 2017
 *      Author: Alwin Abraham
 */
#include "Servo.h"
volatile uint16_t* servo_orcb = (uint16_t*) 0x8080202A;
volatile uint16_t* servo_orca = (uint16_t*) 0x80802028;
int row; int column;
void init_servo() {
	*servo_orca = 1500;
	*servo_orcb = 1400;
	column = 89;
	row = 66;
}
void pan() {
	if (column < 0)
		column = 0;
	else if (column > 255)
		column = 255;
	*servo_orca = column * 10 + 608;
}
void tilt() {
	if (row < 0)
		row = 0;
	else if (row > 255)
		row = 255;
	*servo_orcb = 6.25 * row + 984;
}

