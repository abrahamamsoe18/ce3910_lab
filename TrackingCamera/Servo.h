/*
 * Servo.h
 *
 *  Created on: Mar 16, 2017
 *      Author: Alwin Abraham
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#ifndef SERVO_H_
#define SERVO_H_

/**
 * Value of the Servo A Location
 */
extern volatile uint16_t* servo_orca;
/**
 * Value of the Servo B Location
 */
extern volatile uint16_t* servo_orcb;

/**
 * Move servos to center to start
 */
void init_servo();

/**
 * Pan the motor by using servo_orca
 */
void pan();

/**
 * Tilt the motor by using servo_orcb
 */
void tilt();

#endif /* SERVO_H_ */
