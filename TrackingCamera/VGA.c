/*
 * VGA.c
 *
 *  Created on: Apr 4, 2017
 *      Author: Alwin Abraham
 */

#include "VGA.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

volatile uint8_t* VGA = (uint8_t*) 0x80800000;

void VGA_Test(){
	int row = 0;
	int col = 0;
	uint8_t* memptr;
	memptr = (uint8_t*)(0x800000 +(row<<7));
	for(row=0; row<59; row++){
		for (col = 0; col<79; col++){
			*memptr = 0xF0;
			memptr++;
		}
		memptr = (uint8_t*)(0x800000+(row<<7));
	}
}

void VGA_write_pixel(int row, int col, uint8_t data) {
	uint8_t* memptr;
	memptr = (uint8_t*) (VGA + (row << 7) + (col));
	*memptr = data;
}




