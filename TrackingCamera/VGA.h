/*
 * VGA.h
 *
 *  Created on: Apr 4, 2017
 *      Author: Alwin Abraham
 */
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#ifndef VGA_H_
#define VGA_H_

extern volatile uint8_t* VGA;

/**
 * VGA Test Case
 */
void VGA_Test();

/**
 * Write Pixel to the VGA memory
 * @param row - current row
 * @param col - current col
 * @param data - pixel value
 */
void VGA_write_pixel(int, int, uint8_t);


#endif /* VGA_H_ */
