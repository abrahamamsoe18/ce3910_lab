/**
 * File_Name: main.c
 * @author: Alwin Abraham
 * @version: Week 10
 * @brief: The purpose of this lab is to track a blob
 *         by reading data from a camera and outputting it
 *         to a VGA monitor. Then the camera is to move
 *         with the help of servos to keep the blob on screen
 * @date: March 15
 */
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "Servo.h"
#include "I2C.h"
#include "Camera.h"
#include "VGA.h"

//void write_read_memory(void);
//void commands_to_motor(void);
//void command_I2C(void);

/**
 * Driver for the program. Calls initializers and stays in read camera
 * @return
 */
int main() {
	cameraInit();
	init_servo();
	readCamera();
	return 0;
}

//**PREVIOUS LABS DATA**//
//void write_read_memory() {
//	char input[80];
//	char command[3];
//	uint32_t memory_address = 0;
//	uint32_t additional_param = 0;
//	while (1) {
//		printf("\nEnter Command:");
//		if (fgets(input, sizeof(input), stdin) != NULL) {
//			char *newline = strchr(input, '\n');
//			if (newline != NULL) {
//				*newline = '\0';
//			}
//		}
//		uint32_t num_params = sscanf(input, "%s%x%x", command, &memory_address,
//				&additional_param);
//		if (num_params == 2 && strcmp(command, "RD") == 0) { //Read single
//			printf("0x%x", memory_address);
//			uint32_t *p = (uint32_t*) memory_address;
//			printf("\t%02x", p[0]);
//		} else if (num_params == 3) {
//			if (strcmp(command, "RD") == 0) { //Read Multiple
//				uint32_t *p = (uint32_t*) memory_address;
//				uint32_t i = 0;
//				while (i < additional_param) {
//					if (i % 4 == 0) {
//						printf("\n0x%x:", memory_address + i);
//					}
//					printf("\t%02x", p[i]);
//					i++;
//				}
//			} else if (strcmp(command, "WR") == 0) { //Write to Memory
//				uint32_t *p = (uint32_t *) memory_address;
//				p[0] = additional_param;
//			}
//		}
//	}
//}
//
//void commands_to_motor() {
//	char input[80];
//	char command[5];
//	int rotation_value = 0;
//	init_servo();
//	while (1) {
//		printf("\nEnter Command:");
//		if (fgets(input, sizeof(input), stdin) != NULL) {
//			char *newline = strchr(input, '\n');
//			if (newline != NULL) {
//				*newline = '\0';
//			}
//		}
//		sscanf(input, "%s%i", command, &rotation_value);
//		if (strcmp(command, "PAN") == 0) {
//			pan(rotation_value);
//		} else if (strcmp(command, "TILT") == 0) {
//			tilt(rotation_value);
//		}
//	}
//}
//
//void command_I2C(){
//	char input[80];
//	char command[25];
//	uint32_t register_value = 0;
//	uint32_t data_value = 0;
//	I2Cinit();
//	while (1) {
//		printf("\nEnter Command:");
//		if (fgets(input, sizeof(input), stdin) != NULL) {
//			char *newline = strchr(input, '\n');
//			if (newline != NULL) {
//				*newline = '\0';
//			}
//		}
//		sscanf(input, "%s%i%i", command, &register_value, &data_value);
//		if (strcmp(command, "rdCamReg") == 0) {
//			printf("Camera register => Register: %x = %x", register_value, I2Cread(register_value));
//		} else if (strcmp(command, "wrCamReg") == 0) {
//			I2Cwrite(register_value, data_value);
//			printf("Camera register => Register: %x = %x", register_value, I2Cread(register_value));
//		}
//	}
//}

